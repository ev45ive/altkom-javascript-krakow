
module.exports = {
    entry: './src/index',

    mode: 'development',

    devtool:'sourcemap',

    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
        ]
    }
}