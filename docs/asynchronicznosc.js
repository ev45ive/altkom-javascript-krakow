function echo(message, callback) {
    setTimeout(function () {
        callback(' ... ' + message)
    }, 1500)
}

echo('Anybody there?', function (response) {
    console.log(response)
})

// ==

function echo(message, callback) {
    setTimeout(function () {
        callback(' ... ' + message)
    }, 1000)
}

echo('Pobierz uzytkownika, ', function (response) {
    echo(response + ' pobierz posty, ', function (response) {
        echo(response + ' pobierz komentarze do postów, ', function (response) {
            console.log(response)
        })
    })
})


// ==
function echo(message) {
    return new Promise(function (resolve) {

        setTimeout(function () {
            resolve(' ... ' + message)
        }, 1000)

    })
}

var p = echo('Pobierz uzytkownika, ')

var p2 = p.then(function (response) {
    return echo(response + ' pobierz posty, ', function (response) {
        echo(response + ' pobierz komentarze do postów, ', function (response) {
            console.log(response)
        })
    })
})

// ==
function echo(message, err) {
    return new Promise(function (resolve, reject) {

        setTimeout(function () {
            err && reject('Nie wyszlo... zle zapytanie! ;-( ')
            resolve(' ... ' + message)
        }, 1000)

    })
}

var p = echo('Pobierz uzytkownika, ', true)
    .catch(function (err) {
        return Promise.resolve('Anonimowy uzytkownik')
    })
    .then(function (response) {
        return echo(response + ' pobierz posty, ');
    })
    .then(function (response) {
        return echo(response + ' pobierz posty, ');
    })
    .then(function (response) {
        console.log(response)
    })
    .catch(function (err) { console.log('BLAD:' + err) })

// ==

fetch('http://localhost:8080/products.json')
    .then(function (data) { return data.json() })
    .then(console.log)

// ==

try {

    x.produkty[1]
    console.log('twoj produkt to' + x.produkty[1])

} catch (err) {
    console.log('Nie udalo sie pobrac produktow')
}
// VM1348: 7 Nie udalo sie pobrac produktow

try {
    setTimeout(function () {
        x.produkty[1]

    })
    console.log('twoj produkt to' + x.produkty[1])

} catch (err) {
    console.log('Nie udalo sie pobrac produktow')
// }
// VM1439: 9 Nie udalo sie pobrac produktow

//==

return new Promise(function(resolve){
	resolve(x +2)

	return Promise.resolve(2)
})

// ==