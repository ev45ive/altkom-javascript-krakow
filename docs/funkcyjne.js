tablica = [1, 2, 3, 4, 5];
wyniki = [];

// for(var i in tablica) { liczba = tablica[i]; wyniki.push(liczba*tablica[i+1]);
function pomnozLiczbe(liczba, i, wszystkie) {
    wyniki.push(liczba * wszystkie[i + 1]);
}

tablica.forEach(pomnozLiczbe);

wyniki;

// === 

function MojForEach(tablica, funkcjaCallback) {
    for (var i in tablica) {
        var element = tablica[i]

    }
}

MojForEach([1, 2, 3, 4, 5], function (x) {
    console.log(x * 2);
})

// ==

function MojForEach(tablica, funkcjaCallback) {
    for (var i in tablica) {
        var element = tablica[i]
        funkcjaCallback(element)
    }
}

function razyDwa(x) { console.log(x * 2); }

MojForEach([1, 2, 3, 4, 5], razyDwa)

// ==   
wyniki = []
tablica.forEach(function (liczba) {
    wyniki.push(liczba * 2);
})

// ==

var wyniki = tablica.map(function (elem, index, all) {
    return elem * 2
});
wyniki;
// (5) [2, 4, 6, 8, 10]
tablica
// (5) [1, 2, 3, 4, 5]

// ===
tablica.filter(function (elem, i, all) {
    return elem % 2 !== 0
})
// (3) [1, 3, 5]
// ===
tablica.reduce(function (agg, elem, i, all) {
    console.log(agg, elem, i, all)
    agg = agg + elem;
    return agg
}, 0)
// 15

// ===

[1,2,3,4,5].reduce(function(acc, next){
	acc.sum += next;

    if(next %2 == 0){ acc.even.push(next) }

	return acc;
},{
	even:[],
	sum:0
})

// == 
function pomnoz(x){ return x * 2 }

[1,2,3,4,5].reduce(function(acc, next){
	acc.push( pomnoz(next) )
	return acc;
},[])

// ==


function isEven(x){
	return x % 2 == 0
}

function isNot(fn){
	return function(x){
		return !fn(x)
    }
}

isOdd = isNot(isEven)

// fn => x => !fn(x)

tablica.filter( isOdd )

// ==
// Kombinatory funkcyjne

tablica = [1,2,3,4,5];

function isEven(x){
	return x % 2 == 0
}

function isNot(x){
	return !x
}

function combine( f, g ){
 	return function(x){
 		return f( g( x ) )
 	}
 }

isOdd = combine(isNot, isEven)

tablica.filter( isOdd )
// (3) [1, 3, 5]