// Create
div  = document.createElement('div')

// Modify
div.innerText = "ala ma kota"
div.classList.add('list-group-item')

// Insert
productsListElem.prepend(div)

productsListElem.append(div)

productsListElem.insertBefore(div, productsListElem.children[2])

// Remove
div.remove()


function addDiv(text){
    // Create
    div = document.createElement('div')

    // Modify
    div.innerText = text
    div.classList.add('list-group-item')

    // Insert
    productsListElem.prepend(div)
}
addDiv("ala ma kota")
addDiv("ala ma placki")
addDiv("ala ma i inne problemy...")

// Clone

div2 = div.cloneNode() 
{/* <div class=​"list-group-item">​</div>​ */}


div2 = div.cloneNode(true) 
{/* <div class=​"list-group-item">​Ala ma kota</div>​ */}

// ==

// szukaj wewnatrz 'elem'
elem.querySelector('button')

// czy element spełnia selektor
elem.matches('.list-group-item') == true

// idz w góre (po rodziach) i znajdz najbliższy pasujacy
buttonElem.closest('.list-group-item') 

// == 

{/* <div data-placki-id="123" data-data-urodzin="1970"></div> */}
document.querySelectorAll('[data-placki-id]')
document.querySelector('[data-placki-id]')
document.querySelector('[data-placki-id]').getAttribute('data-placki-id')
// "123"

document.querySelector('[data-placki-id]').dataset
// DOMStringMap {plackiId: "123", dataUrodzin: "1970"}dataUrodzin: "1970"plackiId: "123"__proto__: DOMStringMap
document.querySelector('[data-placki-id]').dataset.plackiId