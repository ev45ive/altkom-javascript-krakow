function createPerson(name) {

    return {
        sayHello: function () {
            return 'I am ' + name
        }
    }

}

var alice = createPerson('Alice')

// ===

function WhatIsThis() {
    console.log(this)
}

window.WhatIsThis()
// VM1964:2 Window {postMessage: ƒ, blur: ƒ, focus: ƒ, close: ƒ, parent: Window, …}
WhatIsThis()
// VM1964:2 Window {postMessage: ƒ, blur: ƒ, focus: ƒ, close: ƒ, parent: Window, …}

var obj = {}
obj.WhatThePlacki = WhatIsThis
obj.WhatThePlacki()
// VM1964:2 {WhatThePlacki: ƒ}

var obj = { name: 'Jestem OBJ!' }
obj.WhatThePlacki = WhatIsThis
obj.WhatThePlacki()
// VM1964:2 {name: "Jestem OBJ!", WhatThePlacki: ƒ}


WhatIsThis()
// VM1964:2 Window {postMessage: ƒ, blur: ƒ, focus: ƒ, close: ƒ, parent: Window, …}

WhatIsThis == obj.WhatThePlacki
true

// ===

function Person(name) {
    this.sayHello = function () { return 'Hi, I am ' + name; }
}

window.sayHello()
"Hi, I am Placki"

var person = Person('Placki')

person
undefined

window.sayHello()
"Hi, I am Placki"

// ==

function Person(name) {
    this.sayHello = function () { return 'Hi, I am ' + name; }
}

var person = new Person('Placki')
person.sayHello()
// "Hi, I am Placki"

// ==
var Pinoccio = {}
Pinoccio.Person = Person

Pinoccio.Person('Pinoccio')

Pinoccio.sayHello()
// "Hi, I am Pinoccio"

// ===
var Pinoccio = {}

Person.call(Pinoccio, 'Pinoccio')

Pinoccio.sayHello()
"Hi, I am Pinoccio"

// ===

function Printable() { this.print = function () { } }
function Serializable() { this.serialize = function () { } }
function Countable() { this.count = function () { } }

var obj = { name: 'Placki', amount: 123 }
obj
// {name: "Placki", amount: 123}

Printable.call(obj, 'argumentA', 'B', 'C')
var argumenty = ['argumentA', 'B', 'C']

Serializable.apply(obj, argumenty)

// {
// amount: 123
// name: "Placki"
// print: ƒ ()
// serialize: ƒ ()
// __proto__: Object
// }

// ===

function Person(name) {
    this.name = name
    this.sayHello = function () { return 'Hi, I am ' + name; }
}

var alice = new Person('Alice')
var bob = new Person('Bob')

alice.sayHello()
"Hi, I am Alice"

bob.sayHello()
"Hi, I am Bob"

// !!
bob.name = 'Kate'
"Kate"

bob.sayHello()
"Hi, I am Bob" // ??

// == 

function Person(name) {
    this.name = name
    this.sayHello = function () { return 'Hi, I am ' + /* >> */  this.name; }
}

var alice = new Person('Alice')
var bob = new Person('Bob')

alice.name = "Dylan"
"Dylan"

alice.sayHello()
"Hi, I am Dylan"

// ==

function Person(name) {
    this.name = name
    this.sayHello = function () { return 'Hi, I am ' + this.name; }
}

var alice = new Person('Alice')
var bob = new Person('Bob')

alice.name == bob.name
false

alice.sayHello == bob.sayHello
false

// ==

function Person(name) {
    this.name = name
    this.sayHello = sayHello
}

function sayHello() { return 'Hi, I am ' + this.name; }

var alice = new Person('Alice')
var bob = new Person('Bob')

alice.sayHello == bob.sayHello
true

alice.sayHello()
"Hi, I am Alice"

bob.sayHello()
"Hi, I am Bob"

// ==

function Person(name) {
    this.name = name
}
Person.prototype.sayHello = function () { return 'Hi, I am ' + this.name; }

var alice = new Person('Alice')
var bob = new Person('Bob')

// ==

function Person(name) {
    this.name = name
}
Person.prototype.sayHello = function () { return 'Hi, I am ' + this.name; }

var alice = new Person('Alice')
var bob = new Person('Bob')

// ƒ (){ return 'Hi, I am '+this.name; }

Person.prototype.sayGoodbye = function () { return 'Hi, I am ' + this.name; }
// ƒ (){ return 'Hi, I am '+this.name; }

alice.sayGoodbye()
"Hi, I am Alice"

// == 

function Person(name) {
    this.name = name
}
Person.prototype.sayHello = function () { return 'Hi, I am ' + this.name; }

var alice = new Person('Alice')

alice
// Person {name: "Alice"}name: "Alice"__proto__: sayGoodbye: ƒ ()sayHello: ƒ ()constructor: ƒ Person(name)__proto__: Object
alice.sayHello = function () { return 'Nie gadam z Tobą!' }

alice.sayHello()
"Nie gadam z Tobą!"

alice
// Person {name: "Alice", sayHello: ƒ}name: "Alice"sayHello: ƒ ()__proto__: sayGoodbye: ƒ ()sayHello: ƒ ()constructor: ƒ Person(name)__proto__: Object
Person.prototype.sayHello.call(this)

// ===

document.querySelectorAll('.col')
// NodeList(2) [div.col, div.col]

document.querySelectorAll('.col').filter


Array.prototype.slice.call(document.querySelectorAll('.col'))

// ==

function Person(name) {
    this.name = name
}
Person.prototype.sayHello = function () { return 'Hi, I am ' + this.name; }

var alice = new Person('Alice')
var bob = new Person('Bob')


function Employee(name, salary) {
    Person.call(this, name)
    this.salary = salary
}
Employee.prototype = Object.create(Person.prototype)
Employee.prototype.work = function () { return 'I need my ' + this.salary; }
Employee.prototype.sayHello = function () { return 'Welcome, my name is ' + this.name; }

var tom = new Employee('Tom', 1300);
tom;

// ==

function MojKonstruktor(){
	this.placki = 123;
	var mojThis = this
	setTimeout(function(){ console.log('1. THIS is :', mojThis, this ) }, 1);

	setTimeout(function(){ console.log('2. THIS is :', this ) }.bind(this) , 1);
}

new MojKonstruktor()

// VM7088:4 1. THIS is : MojKonstruktor {placki: 123} Window {postMessage: ƒ, blur: ƒ, focus: ƒ, close: ƒ, parent: Window, …}
// VM7088:6 2. THIS is : MojKonstruktor {placki: 123}

// ==

function MojKonstruktor(){
	this.placki = 123;
	var mojThis = this

	var funkcja = function(){ console.log('1. THIS is :', mojThis, this ) }

	setTimeout(funkcja, 1);

	setTimeout(funkcja.bind(this) , 1);
}

new MojKonstruktor()

// ==