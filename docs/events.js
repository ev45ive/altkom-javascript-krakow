$0.onclick = function(){
	console.log('No hej ;-)')
}
// == 

setTimeout(function(){ console.log('no Hej!') },   2000);
1
// VM1083:1 no Hej!
setTimeout(function(){ console.log('no Hej!') },   20000);
2
clearTimeout(2)
// undefined
setInterval(function(){ console.log('no Hej!') },   2000);
3
// 10VM1137:1 no Hej!
clearInterval(3)

// ==

setTimeout(function(){ console.log('1') }, 1);

console.log(2);

setTimeout(function(){ console.log('3') }, 0);

console.log(4);


// ==
new Date()
// Thu Jul 25 2019 10:20:49 GMT+0200 (czas środkowoeuropejski letni)
new Date(1970)
// Thu Jan 01 1970 01:00:01 GMT+0100 (czas środkowoeuropejski standardowy)
newDate() - new Date(1970)
// (anonymous) @ VM1739:1
new Date(1950,1,1)
// Wed Feb 01 1950 00:00:00 GMT+0100 (czas środkowoeuropejski standardowy)
new Date(1950,1,1) - 0

// ===

( new Date(1950,1,1)).valueOf()
-628477200000
( new Date(1950,1,1)).toString()
"Wed Feb 01 1950 00:00:00 GMT+0100 (czas środkowoeuropejski standardowy)"
// ==

var start = Date.now()
var i = 0
setInterval(function(){
	i += 100;		
	console.log(Date.now() - start);
},100);

// ==


// document.addEventListener('mousemove',function(event){
$0.addEventListener('mousemove',function(event){
	console.log({
		x: event.x,
		y: event.y,
		offsetX: event.offsetX,
		offsetY: event.offsetY,
		pageX: event.pageX,
		pageY: event.pageY,
		screenX: event.screenX,
		screenY: event.screenY,
    }) 
})