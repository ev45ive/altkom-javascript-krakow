// https://kangax.github.io/compat-table/es6/

// https://babeljs.io/setup#installation

// https://webpack.js.org/concepts/

// npm install webpack-cli 
// npm install -g @webpack-cli/init

// npm install --save-dev babel-loader @babel/core
// npm install @babel/preset-env --save-dev




// ==

defaults = { active: true, selected: false, level: 1 }
intersection = { ...defaults, level: 2, selected: true }

// ==

function runIt(options) {
    const { x, y, z = 1 } = options;

    console.log(x, y, z)
}

runIt({ x: 1, y: 2 })

// ==

function runIt({ x, y, z = 1 }) {

    console.log(x, y, z)
}

runInt({ x: 1, y: 2 })

// == 

function runIt({ x, y, z = 1 }) {

    return { x, y, z }
}

runIt({ x: 1, y: 2 })

// ==

var person = {
    name: 'Alice',
    company: { name: 'ACME' },
    /* address: { street: 'Sezamkowa' }  */
};

function getPersonInfo({
    name,
    company: { name: companyName },
    address: { street } = { street: 'No address' }
}) {
    return ` ${name} ${companyName} ${street} `
};

getPersonInfo(person)

// ==

var person = {
    name: 'Alice',
    company: { name: 'ACME' },
    /* address: { street: 'Sezamkowa' }  */
};

var getPersonInfo = ({
    name,
    company: { name: companyName },
    address: { street } = { street: 'No address' }
}) => ` ${name} ${companyName} ${street} `;

getPersonInfo(person)
" Alice ACME No address "

// ==

var double = x => { return x * 2 }

var double = x => x * 2;