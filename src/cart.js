
function Cart(selector) {
    this._cartElem = $(selector)

    this._cart_items = []

    this._render()
}

Cart.prototype._render = function () {
    var tableRows = $('.js-cart-list tbody').first()
    tableRows.empty()

    // this._cart_items.forEach(functio(elem,indx))
    $.each(this._cart_items, function (index, cart_item) {
        var product = findProductById(cart_item.productId)
        var itemDiv = $('<tr>'
            + '<td>' + product.name + '</td>'
            + '<td>' + product.price + '</td>'
            + '<td>' + cart_item.amount + '</td>'
            + '<td>' + (cart_item.amount * product.price).toFixed(2) + '</td>'
            + '</tr>')
        tableRows.append(itemDiv)
    })
}

Cart.prototype._findByProductId = function (productId) {
    return this._cart_items.filter(function (item) {
        return item.productId == productId
    }).pop()
}

Cart.prototype.getItems = function () {
    return this._cart_items.slice()
}
Cart.prototype.remove = function () { }
Cart.prototype.add = function (productId, amount) {

    // Domyślnie przyjmij ilosc 1
    if (amount === undefined) {
        amount = 1
    }

    // Znajdz pozycje produktu
    var item = this._findByProductId(productId)

    if (item) {
        // Zwieksz istniejaca pozycje
        item.amount += amount
    } else {
        // Dodaj  nowa pozycje
        this._cart_items.push({
            productId: productId,
            amount: amount
        })

    }
    this._render()
}

