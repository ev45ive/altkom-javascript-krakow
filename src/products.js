
var products = []

var selectedProduct = null;
var only_promoted = false;
var products_per_page = 5

var productsListElem = document.querySelector('.js-product-list');


function renderProducts() {
    var displayed_count = 0;
    productsListElem.innerHTML = ''

    for (var i in products) {
        var product = products[i]

        if (only_promoted && !product.promotion) { continue }
        if (displayed_count >= products_per_page) { break; }

        renderProductElem(product)

        displayed_count++
    }

    selectProduct(selectedProduct)
}

productsListElem.addEventListener('click', function (event) {
    var id = event.target.closest('.list-group-item').dataset.productId;
    selectProduct(id)
})

function renderProductElem(product) {
    var div = document.createElement('div')
    // Add visual CSS class 
    div.classList.add('list-group-item')

    // add [data-product-id] attribute to each
    div.dataset.productId = product.id

    // Render text
    div.innerText = renderProductText(product)

    var addToCartButton = document.createElement('button')
    addToCartButton.classList.add('float-right')
    addToCartButton.innerText = "Add to cart"
    div.append(addToCartButton)
    
    addToCartButton.addEventListener('click',function(event){
        event.stopPropagation()
        cart.add(div.dataset.productId)
    })

    // Add to parent
    productsListElem.append(div)
}

function findProductById(id) {
    // return products.find(function (p) { return p.id == id })
    return products.filter(function (p) { return p.id == id }).pop()
}

function selectProduct(id) {
    if (id == null) { return; }
    selectedProduct = id;

    var product = findProductById(selectedProduct)

    // Update details
    renderProductDetails(product)

    // Update list
    productsListElem.querySelectorAll('.list-group-item')
        .forEach(function (elem) {
            // Add class to same ID, remove if different id
            elem.classList.toggle('active', selectedProduct == elem.dataset.productId)
        })
}

function renderProductText(product) {
    var price_gross = (product.price * (1 + product.tax))

    if (product.promotion) {
        var discount_price = price_gross * (1 - product.discount);
        return (product.name + ' ' + discount_price.toFixed(2) + ' PROMOTION! ')
    } else {
        return (product.name + ' ' + price_gross.toFixed(2))
    }
}


function renderProductDetails(product) {
    var elem = document.querySelector('.js-product-details');
    elem.innerHTML = '<dl>'
        + '<dt>Nazwa</dt><dd>' + product.name + '</dd>'
        + '<dt>Cena</dt><dd>' + product.price.toFixed(2) + '</dd>'
        + '</dl>'
}
// ==

document.querySelector('button.refresh-products')
    .addEventListener('click', downloadProducts)

function downloadProducts() {
    return fetchJSONPromise('http://localhost:8080/products.json')
        .then(function (data) {
            products = data;
            renderProducts()
        })
}

function fetchJSONPromise(url) {
    return new Promise(function (resolve, reject) {
        xhr = new XMLHttpRequest()
        xhr.onloadend = function (event) {
            var data = JSON.parse(event.target.response);
            resolve(data)
        }
        xhr.onerror = function (event) {
            reject(event.target.error)
        }
        xhr.open('GET', url)
        xhr.send()
    })

}