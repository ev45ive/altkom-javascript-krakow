import { Cart, CartItem } from './shopping/cart'
import ProductsList, { ProductDetailsView } from './shopping/products';
import $ from 'jquery'

// Export global jQuery
window.$ = $


const cart = new Cart('.js-cart')

const products = new ProductsList('.js-product-list')
const details = new ProductDetailsView('.js-product-details')
const details2 = new ProductDetailsView('.js-product-details2')

products.onSelected(product => {
    details.setProduct(product)
})

products.onSelected(product => {
    details2.setProduct(product)
})


cart.addToCart({
    id: 123, name: 'Test', price: 123
})
// debugger
window.cart = cart
window.products = products

console.log('hello es6 const, let')