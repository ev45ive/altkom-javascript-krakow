
var top_secret = 'plack'
import $ from 'jquery'

export class Cart {

    constructor(selector) {
        this.el = $(selector)
        this.items = {}
    }

    getItems() {
        return Object.values(this.items)
    }

    addToCart(product, amount = 1) {
        var cartItem = this.items[product.id];

        if (!cartItem) {
            cartItem = this.items[product.id] = new CartItem(product)
            this.el.find('tbody').append(cartItem.el)
        }

        cartItem.increaseAmount(amount)
        cartItem.render()
    }

    render() {
        this.el.find('tbody').empty()

        this.getItems().forEach(cartItem => {
            this.el.find('tbody').append(cartItem.el)
        })
    }
}


export class CartItem {

    constructor(product, amount = 0) {
        this.el = $('<tr>')
        this.product = product
        this.amount = amount
    }

    render() {
        this.el.html(`
            <td>${this.product.name}</td>
            <td>${this.product.price}</td>
            <td>${this.amount}</td>
        `)
        return this.el
    }

    increaseAmount(amount) {
        this.amount += amount
    }
}


// Default params:
