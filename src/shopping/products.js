
import $ from 'jquery'


class CollectionView {
    constructor(selector, items = []) {
        this.el = $(selector)
        this.items = items
    }

    render() {
        this.items.forEach(item => item.render())
    }
}


export class ProductDetailsView {
    constructor(selector) {
        this.el = $(selector)        
    }

    setProduct(product) {
        this.data = product.data;
        this.render()
    }

    render() {
        var product = this.data;

        this.el.html('<dl>'
            + '<dt>Nazwa</dt><dd>' + product.name + '</dd>'
            + '<dt>Cena</dt><dd>' + product.price.toFixed(2) + '</dd>'
            + '</dl>')
    }
}


export default class Products extends CollectionView {

    constructor(selector, items = []) {
        super(selector, items)
        this.selectedListeners = $.Callbacks()

        this.el.on('click', '.list-group-item', event => {
            const id = $(event.target).data('product-id')
            this.selectItem(id)
        })
        this.loadProducts()
    }

    selectItem(id) {
        const found = this.items.find(item => item.id == id)
        this.selectedListeners.fire(found)

        this.items.forEach(item => {
            item.setSelected(item.id == id)
        })
    }

    onSelected(callback) {
        this.selectedListeners.add(callback)
    }

    addProduct(productData) {
        const product = new ProductItemView(productData)
        this.items.push(product)
        product.render()
        this.el.append(product.el)
    }

    async loadProducts() {
        const results = await $.getJSON('http://localhost:8080/products.json')
        results.forEach(result => {
            this.addProduct((result))
        })
        return results
    }
}

class ProductItemView {

    constructor(data) {
        this.el = $('<div>')
        this.id = data.id
        this.data = data;
        this.el.data('product-id', this.id)
    }

    setSelected(selected) {
        this.el.toggleClass('active', selected)
    }

    render() {
        const { id, name, price } = this.data
        this.el.addClass('list-group-item')
        this.el.html(`
            ${name} - ${price}
        `)
    }


}